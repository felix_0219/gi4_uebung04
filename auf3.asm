

Fragment_1:
	mov eax, 0
	mov ebx, 1

	inc eax
	jz if

	mov ebx, 0
	jmp end_if	

if:
	mov ebx, 1
end_if:

;-------------------------------------

Fragment_2:
	section .data
	
	a DD 10 DUP(?)

	section .test

	mov edi, 0


for:	cmp edi, 10
	jnb end_for
	
	mov eax, edi
	mul 4
	add eax, a
	mov [eax], edi

	dec edi
	jmp for

end_for:

;--------------------------------------

Fragment_3:
	mov edi, 10
	mov eax, 1

while:
	dec edi
	jz end_while

	mul eax, edi
	jmp while

end_while:	
