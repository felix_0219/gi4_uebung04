section .Data
	n DD 6
	result DD 0

section .text

global _start

_start:

	mov ebx, [n]
	mov eax, 1
loop:
	cmp ebx, 0
	je end
	mul ebx
	dec ebx
	jmp loop
end:
	mov [result], eax
	
	mov rax, 60
	mov rdi, 0
	syscall
