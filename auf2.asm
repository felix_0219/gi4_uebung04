section .Data
	n DD 35
	result DQ 0

section .text

global _start

_start:
	push rbp

	;interator
	mov ebp, [n]
	dec ebp

	;cash
	mov eax, 0
	mov ebx, 0

	;last 
	mov ecx, 0
	mov edx, 1

	;befor last
	mov esi, 0
	mov edi, 0

loop:
	cmp ebp, 0
	je end

	;safe last value
	mov eax, ecx
	mov ebx, edx

	add edx, edi
	adc ecx, esi
	jc overflow

	;restore befor last value
	mov esi, eax
	mov edi, ebx

	dec ebp

	jmp loop
end:
	pop rbp
	
	mov [result], ecx
	mov [result + 4], edx
	
	mov rax, 60
	mov rdi, 0
	syscall

overflow:
	mov ecx, 0
	mov edx, 0
	jmp end
